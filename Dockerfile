FROM debian:unstable-slim

RUN apt update && apt install -y libc-dev gcc g++ clang-4.0 llvm-4.0-dev perl ninja-build cmake libboost-all-dev libnoise-dev gettext python-breathe --no-install-recommends && rm -r /var/lib/apt/lists/*

ADD https://raw.githubusercontent.com/mayah/tinytoml/master/include/toml/toml.h /usr/include/toml/toml.h
RUN chmod 644 /usr/include/toml/toml.h && touch /usr/include/toml/toml.h

RUN ln -s /usr/bin/scan-build-4.0 /usr/bin/scan-build && cp /usr/bin/llvm-cov-4.0 /usr/bin/llvm-cov && cp /usr/bin/llvm-profdata-4.0 /usr/bin/llvm-profdata && cp /usr/bin/clang-4.0 /usr/bin/clang && cp /usr/bin/clang++-4.0 /usr/bin/clang++
